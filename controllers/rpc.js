const bitcoin = require("bitcoinjs-lib");
const { ECPairFactory } = require("ecpair");
const ecc = require("tiny-secp256k1");
const rp = require("request-promise");
const ECPair = ECPairFactory(ecc);
const {faircoinNet,faircoinNode} = require("../config");

exports.getRawTransaction = async (req, res) => {
  try{
    const {txId} = req.body;
    const options = {
      url: faircoinNode.url,
      method: "post",
      headers: {
        "content-type": "text/plain",
      },
      auth: faircoinNode.auth,
      body: JSON.stringify({
        jsonrpc: "1.0",
        id: "curltest",
        method: "getrawtransaction",
        params: [txId],
      }),
    };
  
    try {
      const body = await rp(options);
      const obj = JSON.parse(body);
      if (obj.result) {
        return res.json({data:obj.result});
      } else {
        return res.status(401).json({message:'Can not get getRawTransaction.'});
      }
    } catch (err) {
        return res.status(401).json({message:'Can not get getRawTransaction.'});
    }
  }catch(e){
    return res.status(401).json({message:"Something went wrong"});

  }
  
}

exports.getUtxos = async (req, res) => {
  try{
    const {addr, txIds} = req.body;
    let vins = [];
    let vouts = [];
  
    for (let k = 0; k < txIds.length; k++) {
      let txId = txIds[k];
      const options = {
        url: faircoinNode.url,
        method: "post",
        headers: {
          "content-type": "text/plain",
        },
        auth: faircoinNode.auth,
        body: JSON.stringify({
          jsonrpc: "1.0",
          id: "curltest",
          method: "getrawtransaction",
          params: [txId.addresses, 1],
        }),
      };
    
      try {
        const body = await rp(options);
        const obj = JSON.parse(body);
        if (obj.result && obj.result.vout) {
          for (let i = 0; i < obj.result.vin.length; i++) {
            vins.push({
              tx: obj.result.vin[i].txid,
              id: obj.result.vin[i].vout
            })
          }
  
          for (var i = 0; i < obj.result.vout.length; i++) {
            const out = obj.result.vout[i];
            if (out.scriptPubKey.addresses && out.scriptPubKey.addresses[0] === addr) {
              vouts.push({
                tx: txId.addresses,
                id: out.n
              })
            }
          }
        }
      } catch (err) {
        return res.status(401).json({message:"Failed"});
      }
    }
  
    for (let i = 0; i < vins.length; i++) {
      for (let k = 0; k < vouts.length; k++) {
          if (vins[i].tx == vouts[k].tx && vins[i].id == vouts[k].id) {
              vouts.splice(k, 1);
              break;
          }
      }
    }
  
    // console.log(vouts);
    return res.json({data:vouts});
  }catch(e){
    return res.status(401).json({message:"Something went wrong"});
  }
};

exports.pushtx = async (req, res) => {
  try {
    const {txData} = req.body;
    const options = {
      url: faircoinNode.url,
      method: "post",
      headers: {
        "content-type": "text/plain",
      },
      auth: faircoinNode.auth,
      body: JSON.stringify({
        jsonrpc: "1.0",
        id: "curltest",
        method: "sendrawtransaction",
        params: [txData],
      }),
    };
    try {
      const body = await rp(options);
      const obj = JSON.parse(body);
      if (obj.result) {
        console.log('New Tx',obj.result);
        return res.json({data:obj.result,message:"success"});
      } else {
        return res.status(401).json({message:"Failed in sendrawtransaction."});
      }
    } catch (err) {
      console.log('Failed in sendrawtransaction',err);
      return res.status(401).json({message:"Failed in sendrawtransaction."});
    }
  } 
  catch (error) {
    // console.log(error);
    return res.status(400).json({
      message: 'Something went wrong.'
    });
  }
};
