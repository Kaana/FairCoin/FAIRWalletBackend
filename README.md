# Environment setup
- Install node.js
- npm install
# Run server
- node index.js
# Faircoin node setting
  faircoin node setting located in config.js file.
# Api information
- getRawTransaction
  This api will return raw transaction data for give tx.
  url:http://127.0.0.1/getRawTransaction
  request type: post
  params: {
      txid
  }

  return value:{
      data:#hexcode of txid
  }
- get UTXO
  This api will return UTXO list for give address.
  url:http://127.0.0.1/getUtxos
  request type: post
  params: {
      addr: faircoin address
      txIds: previous tx list
  }

  return value:{
      data:# tx list
  }  
- broadcast transaction
  Broadcast transaction using signed data. This will return transaction hash.
  url:http://127.0.0.1/pushtx
  request type: post
  params: {
      txData: tx data ## singend transaction hex data.
  }

  return value:{
      data: transaction hash
  }    
