const bitcoin = require("bitcoinjs-lib");
const { ECPairFactory } = require("ecpair");
const ecc = require("tiny-secp256k1");

const rp = require("request-promise");

const ECPair = ECPairFactory(ecc);

const faircoinNet = {
  messagePrefix: "\x18Faircoin Signed Message:\n",
  bech32: "tb", // I don't know correct value
  bip32: { public: 70617039, private: 70615956 }, // I don't know correct value
  pubKeyHash: 0x23,
  scriptHash: 0xc4, // I don't know correct value
  wif: 0xd4,
};

async function getRawTransaction(txId) {
  const options = {
    url: "http://27.0.175.48:8332/",
    method: "post",
    headers: {
      "content-type": "text/plain",
    },
    auth: {
      user: "faircoinrpc",
      pass: "GueNA6o5NMX2LXWsDowS2SMXi9C5nYwKTaD21SrAtzAx",
    },
    body: JSON.stringify({
      jsonrpc: "1.0",
      id: "curltest",
      method: "getrawtransaction",
      params: [txId],
    }),
  };

  try {
    body = await rp(options);
  } catch (err) {
    return Promise.reject(err);
  }

  obj = JSON.parse(body);
  if (obj.result) {
    return obj.result;
  } else {
    throw "Invalid txId";
  }
}

async function sendTrnasaction(utxos, receivers, prvKey) {
  var key = ECPair.fromWIF(prvKey, faircoinNet);
  
  const psbt = new bitcoin.Psbt({ network: faircoinNet });

  psbt.setVersion(1);
  psbt.setLocktime(0);

  for (i = 0; i < utxos.length; i++) {
    utxo = utxos[i];

    try {
      prevTxData = await getRawTransaction(utxo.tx);
    } catch (err) {
      return Promise.reject(err);
    }

    if (prevTxData === "") {
      throw new Error("Failed to get txData");
    }

    psbt.addInput({
      hash: utxo.tx,
      index: utxo.id,
      sequence: 0xffffffff,
      nonWitnessUtxo: Buffer.from(prevTxData, "hex"),
    });
  }

  for (i = 0; i < receivers.length; i++) {
    receiver = receivers[i];
    psbt.addOutput({
      address: receiver.address,
      value: receiver.value,
    });
  }

  psbt.signAllInputs(key);

  psbt.finalizeAllInputs();

  txData = psbt.extractTransaction(true).toHex();

  console.log(txData);

  const options = {
    url: "http://27.0.175.48:8332/",
    method: "post",
    headers: {
      "content-type": "text/plain",
    },
    auth: {
      user: "faircoinrpc",
      pass: "GueNA6o5NMX2LXWsDowS2SMXi9C5nYwKTaD21SrAtzAx",
    },
    body: JSON.stringify({
      jsonrpc: "1.0",
      id: "curltest",
      method: "sendrawtransaction",
      params: [txData],
    }),
  };

  try {
    body = await rp(options);
  } catch (err) {
    return Promise.reject(err);
  }

  obj = JSON.parse(body);
  if (obj.result) {
    return obj.result;
  } else {
    throw "Failed to send raw transaction";
  }
}

sendTrnasaction(
  [
    {
      tx: "07eb70f6a6e70a4202aadb5dcece4e5dd3586a48a29c82f0e749522b14ab0f85",
      id: 0,
    }
  ],
  [{ address: "F8jLegBJhmWBEKB68EDW9dpq2ZWdBt9qPc", value: 9000000 }],
  "YN9stgoARVVoFwg8hpRrrioLGhw1ydtPGpdptXYhSm64Tr1SWsdz"
)
  .then((txId) => console.log("tx id: ", txId))
  .catch((err) => console.log("Err:", err));
