const jwt = require('jsonwebtoken');
const RPC_CTR = require('./controllers/rpc');
const router = require('express').Router();
const path = require('path');

router.post('/getRawTransaction', RPC_CTR.getRawTransaction);
router.post('/getUtxos', RPC_CTR.getUtxos);
router.post('/pushtx', RPC_CTR.pushtx);

module.exports = (app, io) => {
  app.use('/api', router);
  app.get('*', function (req, res) {
    // console.log(req);
    res.sendFile(path.resolve(__dirname, 'build', 'index.html'));
  });
  
  app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
  });

  app.use((error, req, res, next) => {
    res.status(error.status || 500).json({
      message: error.message
    });
  });
};
