module.exports = {
  port: process.env.PORT || 3000,
  db: {
    prod: process.env.DATABASE_URL || 'mongodb://localhost/bot',
    test: 'mongodb://localhost/bot',
    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true
    }
  },
  jwt: {
    //for players credential
    secret: process.env.JWT_SECRET || 'development_secret',
    expiry: '1d'
  },
  credentials:{
    //for provider credential
    expiry: 10
  },
  faircoinNode:{
    url: "http://27.0.175.48:8332/",
    auth: {
      user: "faircoinrpc",
      pass: "GueNA6o5NMX2LXWsDowS2SMXi9C5nYwKTaD21SrAtzAx",
    },
  },
  faircoinNet:{
    messagePrefix: "\x18Faircoin Signed Message:\n",
    bech32: "tb", // I don't know correct value
    bip32: { public: 70617039, private: 70615956 }, // I don't know correct value
    pubKeyHash: 0x23,
    scriptHash: 0xc4, // I don't know correct value
    wif: 0xd4,
  },
};
